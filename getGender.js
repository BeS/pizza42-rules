function GetGenderByGivenName(user, context, callback) {
  const request = require('request');
  
  // We could as well call the Google Profile API here and retrieve the Google
  // user's gender from there (given that he set the privacy settings to public at 
  // https://myaccount.google.com/u/1/gender, however, this more
  // generic approach using a gender API works for other connection types
  // as well, where there is no gender returned by default.
  
  // Optional checks, not implemented:
  // Persist the gender to avoid unnecessary API requests / latency.
  // Check if gender already exists in the user profile.
  
  let firstname = user.given_name;
  if (!firstname) {
    firstname = user.name.split(' ')[0];
  }
  
  if (firstname) {
    
    request.get('https://api.genderize.io/?name=' + firstname, {
        json: true
      },
      (err, response, body) => {
        if (err) return callback(err);

        if (response.statusCode === 200) {
          context.idToken['https://techchallenge/gender'] = body.gender;
        }
        return callback(null, user, context);
      });   
    
  } else {
    return callback(null, user, context);    
  }

}
