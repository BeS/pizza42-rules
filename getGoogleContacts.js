function GetGoogleContacts(user, context, callback) {
    const request = require('request');
    
    let googleAccounts = user.identities.filter(id => id.provider === 'google-oauth2');
      
    if (googleAccounts.length === 1) {
      
      let googleAccessToken = googleAccounts[0].access_token;
      
      request.get('https://people.googleapis.com/v1/people/me/connections?personFields=names,emailAddresses', {
          json: true,
          headers: {
              Authorization: ' Bearer ' + googleAccessToken
            }
        },
        (err, response, body) => {
            if (err) {
                return callback(err);
            }
            if (response.statusCode === 200) {
                context.idToken['https://techchallenge/contacts'] = body.totalPeople;
                user.app_metadata = user.app_metadata || {};
                // short-circuit if the contacts count didn't changed
                if (user.app_metadata.contacts && user.app_metadata.contacts === body.totalPeople) return callback(null, user, context);
                // if the contacts count changed we update the metadata
                user.app_metadata.contacts = body.totalPeople;
                auth0.users.updateAppMetadata(user.user_id, user.app_metadata)
                .then(function() {
                    callback(null, user, context);
                })
                .catch(function(err){
                    callback(err);
                });
            }
            return callback(null, user, context);
        });

    } else {
        return callback(null, user, context);    
    }
}
